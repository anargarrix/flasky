from blog import create_app,db
from flask import current_app

import os
import unittest

BASE_DIR = os.path.abspath(os.path.dirname(__file__))

TEST_DB = 'test.db'
 
 

class BasicsTestCase(unittest.TestCase):

    def setUp(self):
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()
        self.client = self.app.test_client(use_cookies=True)

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_home_page(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertTrue('Witcher' in response.get_data(as_text=True))
 
if __name__ == "__main__":
    unittest.main()