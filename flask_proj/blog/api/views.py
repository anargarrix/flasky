from flask import Blueprint,jsonify,request
from ..models import ProductSchema,Product
from blog import db


api = Blueprint('api',__name__)

product_schema = ProductSchema()
products_schema = ProductSchema(many=True)


@api.route('/product',methods =['POST'])
def create_product():
    name = request.json['name']
    desc = request.json['desc']
    price = request.json['price']
    qty = request.json['qty']

    product = Product(name=name,desc=desc,price=price,qty=qty)
    db.session.add(product)
    db.session.commit()

    return product_schema.jsonify(product)

@api.route('/products',methods=["GET"])
def get_products():
    products = Product.query.all()
    response = products_schema.dump(products)
    return jsonify(response)

@api.route('/product/<int:id>',methods=['GET'])
def get_product(id):
    product = Product.query.get(id)
    return product_schema.jsonify(product)

@api.route('/product/<int:id>',methods=['POST'])
def update_product(id):
    product = Product.query.get(id)
    product.name = request.json['name']
    product.desc = request.json['desc']
    product.price = request.json['price']
    product.qty = request.json['qty']
    db.session.commit()
    return product_schema.jsonify(product)

@api.route('/product/<int:id>',methods=['DELETE'])
def delete_product(id):
    product = Product.query.get(id)
    db.session.delete(product)
    db.session.commit()
    return jsonify(message="you deleetd succesfully")