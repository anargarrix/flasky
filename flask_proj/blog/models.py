from blog import db,login_manager,ma
from werkzeug.security import generate_password_hash,check_password_hash
from flask_login import UserMixin
from datetime import datetime

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(user_id)


class User(db.Model,UserMixin):

    __tablename__ = 'users'
    
    id = db.Column(db.Integer,primary_key=True)
    profile_image = db.Column(db.String(20),nullable=False,default='default.png')
    email = db.Column(db.String(64),unique=True,index=True)
    username = db.Column(db.String(120),unique=True,index=True)
    password_hash = db.Column(db.String(64))

    posts = db.relationship('BlogPost',backref='author',lazy=True)

    def __init__(self,email,username,password):
        self.email = email
        self.username = username
        self.password_hash = generate_password_hash(password)  

    def check_password(self,password):
        return check_password_hash(self.password_hash,password)
    
    def __repr__(self):
        return f'Username {self.username}'

    def bulk(self):
        return self.query.all()

class BlogPost(db.Model):

    id = db.Column(db.Integer,primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'),
        nullable=True)

    date = db.Column(db.DateTime,nullable=False,default=datetime.utcnow)
    title = db.Column(db.String(140),nullable=False)
    text = db.Column(db.Text,nullable=False)

    def __init__(self,title,text,user_id):
        self.title = title
        self.text = text
        self.user_id = user_id

    def __repr__(self):
        return f'Post title - {self.title}'


class Product(db.Model):
    id = db.Column(db.Integer,primary_key=True)
    name = db.Column(db.String(128))
    desc = db.Column(db.String(128))
    price = db.Column(db.Float)
    qty = db.Column(db.Integer)

    def __init__(self, name,desc,price,qty):
        self.name = name
        self.desc = desc
        self.price = price
        self.qty = qty

class ProductSchema(ma.Schema):
    class Meta:
        fields = ['name','desc','price','qty']