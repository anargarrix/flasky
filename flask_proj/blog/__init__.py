
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_login import LoginManager
from config import config


login_manager = LoginManager()
login_manager.login_view = 'users.login'

ma = Marshmallow()

db = SQLAlchemy()


def create_app(config_name):
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)

    
    db.init_app(app)
    ma.init_app(app)
   

    from blog.core.views import core
    from blog.error_pages.handlers import error_pages
    from blog.user.views import users
    from blog.api.views import api
    app.register_blueprint(core)
    app.register_blueprint(users)
    app.register_blueprint(api,url_prefix='/api')
    app.register_blueprint(error_pages)


    return app

