from flask_wtf import FlaskForm
from wtforms import StringField,PasswordField,SubmitField
from wtforms.validators import DataRequired,Email,EqualTo
from wtforms import ValidationError
from flask_wtf.file import FileField,FileAllowed
from flask_login import current_user
from blog.models import User

class LoginForm(FlaskForm):
    email = StringField('Email',validators=[DataRequired(),Email()])
    password = PasswordField('Password',validators=[DataRequired()])
    submit  = SubmitField('Submit')


class RegistrationForm(FlaskForm):
    email = StringField('Email',validators=[DataRequired(),Email()])
    username = StringField('UserName',validators=[DataRequired()])
    password = PasswordField('Password',validators=[DataRequired(),EqualTo('pass_confirm',message='Passwords must match')])
    pass_confirm = PasswordField('Confirm Password',validators=[DataRequired()])

    def check_email(self,field):
        if User.query.filter(email=field.data).first():
            raise ValidationError('Your email has been registered already')
    
    def check_username(self,field):
        if User.query.filter(username=field.data).first():
            raise ValidationError('Your username has been registered already')


class UpdateUserForm(FlaskForm):
    email = StringField('Email',validators=[DataRequired(),Email()])
    username = StringField('UserName',validators=[DataRequired()])
    picture = FileField('Update Profile Picture',validators=[FileAllowed(['jpg','png'])])
    submit  = SubmitField('Submit')


    def validate_email(self,field):
        if User.query.filter(email=field.data).first():
            raise ValidationError('Your email has been registered already')
    
    def validate_username(self,field):
        if User.query.filter(username=field.data).first():
            raise ValidationError('Your username has been registered already')